(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)
//
(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"
#staload _ =
"./../../mylib/mylib.dats"

(* ****** ****** *)

#staload "./project.sats"

(* ****** ****** *)

extern
fun
tmvar_new0(): tmvar
extern
fun
tmvar_new1(t0var): tmvar

extern
fun
eq_tmvar_tmvar
(tmvar, tmvar): bool
overload = with eq_tmvar_tmvar

(* ****** ****** *)

local

datatype
tmvar =
TMVAR of
(t0var, int(*stamp*))

absimpl tmvar_type = tmvar

val theStamp = ref<int>(0)

fun
theStamp_get(): int =
let
val n = theStamp[] in theStamp[] := n+1; n 
end

in

implement
tmvar_new1(id) =
TMVAR(id, stamp) where
{
  val stamp = theStamp_get()
}

implement
eq_tmvar_tmvar(x, y) =
let
val TMVAR(_, x) = x
val TMVAR(_, y) = y in x = y
end

(* ****** ****** *)
//
implement
print_tmvar
  (x0) =
fprint_tmvar(stdout_ref, x0)
//
implement
fprint_tmvar
  (out, x0) =
(
case+ x0 of
| TMVAR(nam, idx) =>
  fprint!(out, nam, "(", idx, ")")
)
//
end // end of [local]

(* ****** ****** *)

implement
print_t1erm(x0) =
fprint_t1erm(stdout_ref, x0)
implement
print_t1dcl(x0) =
fprint_t1dcl(stdout_ref, x0)
implement
print_p1grm(x0) =
fprint_p1grm(stdout_ref, x0)

(* ****** ****** *)

implement
fprint_val<t1erm> = fprint_t1erm
implement
fprint_val<t1dcl> = fprint_t1dcl

(* ****** ****** *)

implement
fprint_t1erm(out, x0) =
(
case- x0 of
| T1Mnil() =>
  fprint!(out, "T1Mnil(", ")")
| T1Mint(i0) =>
  fprint!(out, "T1Mint(", i0, ")")
| T1Mbtf(b0) =>
  fprint!(out, "T1Mbtf(", b0, ")")
| T1Mstr(s0) =>
  fprint!(out, "T1Mstr(", s0, ")")
| T1Mvar(x0) =>
  fprint!(out, "T1Mvar(", x0, ")")
)

(* ****** ****** *)

implement
fprint_t1dcl(out, x0) =
(
case+ x0 of
| T1DCL(tmv, t1m) =>
  fprint!(out, "T1DCL(", tmv, "; ", t1m, ")")
) (* end of [fprint_t1dcl] *)

(* ****** ****** *)

implement
fprint_p1grm(out, x0) =
(
case+ x0 of
| P1GRM(t1ds, t1m0) =>
  fprint!(out, "P1GRM(", t1ds, "; ", t1m0, ")")
) (* end of [fprint_p1grm] *)

(* ****** ****** *)

abstype t0env_type = ptr
typedef t0env = t0env_type

(* ****** ****** *)
//
extern
fun
t0env_make_nil(): t0env
//
extern
fun
t0env_extend
( t0env
, t0var, tmvar): t0env
extern
fun
t0env_search
(t0env, t0var): myoptn(tmvar)
//
(* ****** ****** *)

local

absimpl
t0env_type =
mylist@(t0var, tmvar)

in (*in-of-local*)

implement
t0env_make_nil
((*void*)) = mylist_nil()

implement
t0env_extend
(env, x0, x1) =
mylist_cons((x0, x1), env)

implement
t0env_search
  (env, x0) =
(
  auxlst(env)
) where
{
fun
auxlst
( xts
: t0env
)
: myoptn(tmvar) =
(
case+ xts of
| mylist_nil() =>
  myoptn_none()
| mylist_cons(xt1, xts) =>
  if
  (xt1.0 = x0)
  then myoptn_some(xt1.1) else auxlst(xts)
)
} (* end of [t0env_search] *)

end // end of [local]

(* ****** ****** *)

local

fun
auxterm
( env0
: t0env
, t0m0
: t0erm): t1erm =
(
case- t0m0 of
//
| T0Mnil() => T1Mnil()
| T0Mint(i0) => T1Mint(i0)
| T0Mbtf(b0) => T1Mbtf(b0)
| T0Mstr(s0) => T1Mstr(s0)
//
| T0Mvar(t0v) =>
  let
  val opt =
  t0env_search(env0, t0v)
  val () =
  println!
  ("auxterm: t0v = ", t0v)
  in
  case- opt of
  | myoptn_some(tmv) => T1Mvar(tmv)
  end
//
| T0Mapp
  (t0m1, t0m2) =>
  (
    T1Mapp(t1m1, t1m2)
  ) where
  {
    val
    t1m1 = auxterm(env0, t0m1)
    val
    t1m2 = auxterm(env0, t0m2)
  }
//
| T0Mlam
  (t0v, opt, body) =>
  let
    val tmv =
    tmvar_new1(t0v)
    val env1 =
    t0env_extend(env0, t0v, tmv)
    val body =
    auxterm(env1, body)
  in
    T1Mlam(tmv, opt, body)
  end
//
| T0Mlet(tdcs, t0m1) =>
  let
    val
    (tdcs, t1m1) =
    auxtdcs
    (env0, tdcs, t0m1) in T1Mlet(tdcs, t1m1)
  end
//
) (* end of [auxterm] *)

and
auxtdcs
( env0
: t0env
, tdcs
: t0dclist
, t0m1: t0erm
)
: (t1dclist, t1erm) =
(
case+ tdcs of
|
mylist_nil() =>
(tdcs, t1m1) where
{
  val tdcs = mylist_nil()
  val t1m1 = auxterm(env0, t0m1)
}
|
mylist_cons(tdc0, tdcs) =>
let
  val
  T0DCL(t0v, t0m) = tdc0
  val tmv = tmvar_new1(t0v)
  val t1m = auxterm(env0, t0m)
  val tdc0 = T1DCL(tmv, t1m)
  val
  (tdcs, t1m1) =
  (
  auxtdcs(env1, tdcs, t0m1)
  ) where
  {
    val env1 =
    t0env_extend(env0, t0v, tmv)
  }
in
  (mylist_cons(tdc0, tdcs), t1m1)
end
) (* end of [auxtdcs] *)
  
in(*in-of-local*)

implement
trans01_term
  (t0m0) =
(
  auxterm(env0, t0m0)
) where
{
  val env0 = t0env_make_nil((*void*))
}

implement
trans01_pgrm
  (p0gm) =
(
case+ p0gm of
|
P0GRM(tdcs, t0m0) =>
(
  P1GRM(tdcs, t1m0)
) where
{
//
  val env0 = t0env_make_nil((*void*))
//
  val
  (tdcs, t1m0) = auxtdcs(env0, tdcs, t0m0)
//
} (* where *)
) (* end of [trans01_pgrm] *)


end // end of [local]

(* ****** ****** *)

(* end of [project_t1erm.dats] *)
