(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)
//
(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"
#staload _ =
"./../../mylib/mylib.dats"

(* ****** ****** *)

#staload "./project.sats"

(* ****** ****** *)

extern
fun
t2erm_make
(type2, t2erm_node): t2erm

extern
fun
t2erm_get_type(t2erm): type2
extern
fun
t2erm_get_node(t2erm): t2erm_node
overload .type with t2erm_get_type
overload .node with t2erm_get_node

(* ****** ****** *)

(*
local

datatype t2erm =
T2ERM of
(type2, t2erm_node)

absimpl t2erm_type = t2erm

in(*in-of-local*)

implement
t2erm_make
(t2p0, node) = T2ERM(t2p0, node)

implement
t2erm_get_type
(t2m0) =
let
val T2ERM(t2p0, node) = t2m0 in t2p0
end
implement
t2erm_get_node
(t2m0) =
let
val T2ERM(t2p0, node) = t2m0 in node
end

end // end of [local]
*)

(* ****** ****** *)

local

absimpl
t2erm_type =
$rec{
  node= t2erm_node, type= type2
}

in(*in-of-local*)

implement
t2erm_make
(t2p0, node) =
$rec{node=node,type=t2p0}

implement
t2erm_get_type(t2m0) = t2m0.type
implement
t2erm_get_node(t2m0) = t2m0.node

end // end of [local]

(* ****** ****** *)

abstype t1env_type = ptr
typedef t1env = t1env_type

(* ****** ****** *)
//
extern
fun
t1env_make_nil(): t1env
//
extern
fun
t1env_extend
( t1env
, tmvar, type2): t1env
extern
fun
t1env_search
(t1env, tmvar): myoptn(type2)
//
(* ****** ****** *)

local

absimpl
t1env_type =
mylist@(tmvar, type2)

in (*in-of-local*)

implement
t1env_make_nil
((*void*)) = mylist_nil()

implement
t1env_extend
(env, x0, tp) =
mylist_cons((x0, tp), env)

implement
t1env_search
  (env, x0) = myoptn_none() // placeholder

end // end of [local]

(* ****** ****** *)

val T2Pnil = T2Pbas("nil")
val T2Pint = T2Pbas("int")
val T2Pbtf = T2Pbas("bool")
val T2Pstr = T2Pbas("string")

(* ****** ****** *)

local

fun
auxterm
( env0
: t1env
, t1m0
: t1erm): t2erm =
(
case- t1m0 of
//
| T1Mnil() =>
  t2erm_make(T2Pnil, T2Mnil())
| T1Mint(i0) =>
  t2erm_make(T2Pint, T2Mint(i0))
| T1Mbtf(b0) =>
  t2erm_make(T2Pbtf, T2Mbtf(b0))
| T1Mstr(s0) =>
  t2erm_make(T2Pstr, T2Mstr(s0))
//
) (* end of [auxterm] *)

in(*in-of-local*)

implement
trans12_term
  (t1m0) =
(
  auxterm(env0, t1m0)
) where
{
  val env0 = t1env_make_nil((*void*))
}

end // end of [local]

(* ****** ****** *)

(* end of [project_t2erm.dats] *)
