(*
** For your
** final project
*)

(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"

(* ****** ****** *)
//
typedef tpnam = string
//
(* ****** ****** *)
//
datatype
type0 =
//
| T0Pbas of tpnam // base
//
| T0Ptup of // tuples
  (type0(*fst*), type0(*snd*))
//
| T0Pfun of // functions
  (type0(*arg*), type0(*res*))
//
typedef type0opt = myoptn(type0)
typedef type0lst = mylist(type0)
//
(* ****** ****** *)
//
datatype
ctype0 =
  | CTP of (type0lst, type0)
//
(* ****** ****** *)
//
fun
print_type0: print_type(type0)
fun
fprint_type0: fprint_type(type0)
//
overload print with print_type0
overload fprint with fprint_type0
//
fun
print_ctype0: print_type(ctype0)
fun
fprint_ctype0: fprint_type(ctype0)
//
overload print with print_ctype0
overload fprint with fprint_ctype0
//
(* ****** ****** *)

typedef t0var = string
typedef oprnm = string

(* ****** ****** *)

datatype
t0dcl =
| T0DCL of
  (t0var, t0erm)
//
and t0erm =
//
  | T0Mnil of () // for voids
  | T0Mint of int // for integers
  | T0Mbtf of bool // for booleans
  | T0Mstr of string // for strings
//
  | T0Mvar of t0var
  | T0Mlam of
    (t0var, type0opt, t0erm)
  | T0Mapp of (t0erm, t0erm)
//
  | T0Mift of (t0erm, t0erm, t0ermopt)
//
  | T0Mopr of (oprnm, t0ermlst)
//
  | T0Mtup of (t0erm, t0erm)
  | T0Mfst of t0erm | T0Msnd of t0erm
//
  | T0Mann of (t0erm, type0) // type-annotation
//
  | T0Mlet of
    (t0dclist(*local*), t0erm(*scope*)) // local bindings
//
  | T0Mfix of
    ( t0var(*f*)
    , t0var(*x*)
    , type0opt(*arg*)
    , type0opt(*res*), t0erm) // Y(lam f.lam x.<body>)
//
where
t0dclist = mylist(t0dcl)
and
t0ermlst = mylist(t0erm)
and
t0ermopt = myoptn(t0erm)

(* ****** ****** *)

datatype p0grm =
| P0GRM of (t0dclist, t0erm)

(* ****** ****** *)
//
fun
print_t0erm: print_type(t0erm)
fun
fprint_t0erm: fprint_type(t0erm)
//
overload print with print_t0erm
overload fprint with fprint_t0erm
//
fun
print_t0dcl: print_type(t0dcl)
fun
fprint_t0dcl: fprint_type(t0dcl)
//
overload print with print_t0dcl
overload fprint with fprint_t0dcl
//
fun
print_p0grm: print_type(p0grm)
fun
fprint_p0grm: fprint_type(p0grm)
//
overload print with print_p0grm
overload fprint with fprint_p0grm
//
(* ****** ****** *)
//
// Resolving binding/scoping issues
//
(* ****** ****** *)

abstype tmvar_type = ptr
typedef tmvar = tmvar_type

(* ****** ****** *)
//
fun
print_tmvar(tmvar): void
fun
fprint_tmvar: fprint_type(tmvar)
//
overload print with print_tmvar
overload fprint with fprint_tmvar
//
(* ****** ****** *)

datatype
t1dcl =
| T1DCL of
  (tmvar, t1erm)
//
and t1erm =
//
  | T1Mnil of ()
  | T1Mint of int
  | T1Mbtf of bool
  | T1Mstr of string
//
  | T1Mvar of tmvar
  | T1Mlam of
    (tmvar, type0opt, t1erm)
  | T1Mapp of (t1erm, t1erm)
//
  | T1Mift of (t1erm, t1erm, t1ermopt)
//
  | T1Mopr of (oprnm, t1ermlst)
//
  | T1Mtup of (t1erm, t1erm)
  | T1Mfst of t1erm | T1Msnd of t1erm
//
  | T1Mann of (t1erm, type0) // type-annotation
//
  | T1Mlet of
    (t1dclist(*local*), t1erm(*scope*)) // local bindings
//
  | T1Mfix of
    ( tmvar(*f*)
    , tmvar(*x*)
    , type0opt(*arg*)
    , type0opt(*res*), t1erm) // Y(lam f.lam x.<body>)
//
where
t1dclist = mylist(t1dcl)
and
t1ermlst = mylist(t1erm)
and
t1ermopt = myoptn(t1erm)

(* ****** ****** *)

datatype p1grm =
| P1GRM of (t1dclist, t1erm)

(* ****** ****** *)
//
fun
print_t1erm: print_type(t1erm)
fun
fprint_t1erm: fprint_type(t1erm)
//
overload print with print_t1erm
overload fprint with fprint_t1erm
//
fun
print_t1dcl: print_type(t1dcl)
fun
fprint_t1dcl: fprint_type(t1dcl)
//
overload print with print_t1dcl
overload fprint with fprint_t1dcl
//
fun
print_p1grm: print_type(p1grm)
fun
fprint_p1grm: fprint_type(p1grm)
//
overload print with print_p1grm
overload fprint with fprint_p1grm
//
(* ****** ****** *)

fun trans01_term : t0erm -> t1erm
fun trans01_pgrm : p0grm -> p1grm

(* ****** ****** *)
//
abstype tpext_type = ptr
typedef tpext = tpext_type
//
(* ****** ****** *)
//
datatype
type2 =
//
| T2Pbas of tpnam // base
//
| T2Ptup of // tuples
  (type2(*fst*), type2(*snd*))
//
| T2Pfun of // functions
  (type2(*arg*), type2(*res*))
//
| T2Pext of tpext // existential
//
typedef type2opt = myoptn(type2)
typedef type2lst = mylist(type2)
//
(* ****** ****** *)
//
datatype
ctype2 =
  | CTP of (type2lst, type2)
//
(* ****** ****** *)
//
fun
print_type2: print_type(type2)
fun
fprint_type2: fprint_type(type2)
//
overload print with print_type2
overload fprint with fprint_type2
//
fun
print_ctype2: print_type(ctype2)
fun
fprint_ctype2: fprint_type(ctype2)
//
overload print with print_ctype2
overload fprint with fprint_ctype2
//
(* ****** ****** *)

abstype t2erm_type = ptr
typedef t2erm = t2erm_type

(*
absimpl
t2erm_type =
$rec{
  t2erm_type= type2
, t2erm_node= t2erm_node
}
*)

(* ****** ****** *)

datatype
t2dcl =
| T2DCL of
  (tmvar, t2erm)
//
and t2erm_node =
//
  | T2Mnil of ()
  | T2Mint of int
  | T2Mbtf of bool
  | T2Mstr of string
//
  | T2Mvar of tmvar
  | T2Mlam of
    (tmvar, type0opt, t2erm)
  | T2Mapp of (t2erm, t2erm)
//
  | T2Mift of (t2erm, t2erm, t2ermopt)
//
  | T2Mopr of (oprnm, t2ermlst)
//
  | T2Mtup of (t2erm, t2erm)
  | T2Mfst of t2erm | T2Msnd of t2erm
//
  | T2Mann of (t2erm, type0) // type-annotation
//
  | T2Mlet of
    (t2dclist(*local*), t2erm(*scope*)) // local bindings
//
  | T2Mfix of
    ( tmvar(*f*)
    , tmvar(*x*)
    , type0opt(*arg*)
    , type0opt(*res*), t2erm) // Y(lam f.lam x.<body>)
//
  | T2Mcast of (t2erm, type0) // for internalization of type-errors
//
where
t2dclist = mylist(t2dcl)
and
t2ermlst = mylist(t2erm)
and
t2ermopt = myoptn(t2erm)

(* ****** ****** *)

datatype p2grm =
| P2GRM of (t2dclist, t2erm)

(* ****** ****** *)

fun trans12_term : t1erm -> t2erm
fun trans12_pgrm : p1grm -> p2grm

(* ****** ****** *)

datatype
t3reg = T3REG of int(*stamp*)

(* ****** ****** *)

fun
t3reg_new(): t3reg

(* ****** ****** *)

datatype
t3val =
| T3Vint of int
| T3Vstr of string
| T3Vfun of string
| T3Vtmp of t3reg
| T3Varg of string
| T3Venv of (string, int)
| T3Vclo of (string, t3vals(*env*), t3inss(*body*), t3val(*res*))

and
t3ins =
| T3Imov of (t3reg, t3val)
| T3Itup of (t3reg, t3val, t3val)
| T3Ical of (t3reg, t3val, t3val)
| T3Iopr of (t3reg, oprnm, t3vals)
| T3Iift of (t3val, t3inss, t3inss)

where
t3vals = mylist(t3val)
and
t3inss = mylist(t3ins)

(* ****** ****** *)

abstype
t3env_type = ptr
typedef
t3env = t3env_type

fun
compile :
(t3env, t2erm) -> (t3inss, t3val)

fun
compile_var :
(t3env, t2erm) -> t3val
and
compile_lam :
(t3env, t2erm) -> t3val
and
compile_fix :
(t3env, t2erm) -> t3val

(* ****** ****** *)

fun emit_tval : t3val -> void
fun emit_tins : t3ins -> void
fun emit_tinss : t3inss -> void

(* ****** ****** *)
//
fun
project_main0
{n:int | n >= 1}(int(n), !argv(n)): void
//
(* ****** ****** *)

(* end of [project.sats] *)
