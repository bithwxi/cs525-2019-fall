(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "./../mylib/mylib.sats"

(* ****** ****** *)

typedef tnam = string
typedef vnam = string
typedef opnm = string

(* ****** ****** *)
//
datatype type =
  | TPbas of tnam
  | TPfun of
    (type(*arg*), type(*res*))
  | TPtup of
    (type(*fst*), type(*snd*))
//
(* ****** ****** *)
//
extern
fun
eq_type_type:
(type, type) -> bool
overload = with eq_type_type
//
(* ****** ****** *)

extern
fun
print_type(type): void // stdout
and
prerr_type(type): void // stderr
and
fprint_type(FILEref, type): void

overload print with print_type
overload prerr with prerr_type
overload fprint with fprint_type

(* ****** ****** *)

datatype expr = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of
    (vnam, type, expr)
  | TMapp of (expr, expr)
//
  | TMifz of (expr, expr, expr)
//
  | TMfix of (vnam, vnam, expr) // Y(lam f.lam x.<body>)
//
  | TMopr of (opnm, exprlst)
//
  | TMtup of (expr, expr)
  | TMfst of expr | TMsnd of expr
//
  | TMlet of (vnam, expr, expr) // let x = t1 in t2 end
//
where exprlst = list0(expr)

(* ****** ****** *)

extern
fun
print_expr(expr): void // stdout
and
prerr_expr(expr): void // stderr
and
fprint_expr(FILEref, expr): void

overload print with print_expr
overload prerr with prerr_expr
overload fprint with fprint_expr

(* ****** ****** *)

implement
eq_type_type =
lam(tp1, tp2) =>
(
case+
(tp1, tp2) of
| (TPbas nm1,
   TPbas nm2) => (nm1 = nm2)
| (TPfun(tp11, tp12), 
   TPfun(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (TPtup(tp11, tp12), 
   TPtup(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (_, _) => false
)

(* ****** ****** *)

implement
print_type(tp) =
fprint_type(stdout_ref, tp)
implement
prerr_type(tp) =
fprint_type(stderr_ref, tp)

(* ****** ****** *)

implement
fprint_type(out, tp0) =
(
case+ tp0 of
| TPbas(nm) =>
  fprint!(out, "TPbas(", nm, ")")
| TPfun(tp1, tp2) =>
  fprint!(out, "TPfun(", tp1, ", ", tp2, ")")
| TPtup(tp1, tp2) =>
  fprint!(out, "TPtup(", tp1, ", ", tp2, ")")
)

(* ****** ****** *)

implement
print_expr(tm) =
fprint_expr(stdout_ref, tm)
implement
prerr_expr(tm) =
fprint_expr(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<expr> = fprint_expr

(* ****** ****** *)

implement
fprint_expr(out, tm0) =
(
case+ tm0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMstr(x) =>
  fprint!(out, "TMstr(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tp0, tm1) =>
  fprint!
  ( out
  , "TMlam(", x, "; ", tp0, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, x, tm1) =>
  fprint!(out, "TMfix(", f, "; ", x, "; ", tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
//
| TMfst(tm1) =>
  fprint!(out, "TMfst(", tm1, ")")
| TMsnd(tm1) =>
  fprint!(out, "TMsnd(", tm1, ")")
| TMtup(tm1, tm2) =>
  fprint!(out, "TMtup(", tm1, "; ", tm2, ")")
//
| TMlet(x0, tm1, tm2) =>
  fprint!(out, "TMlet(", x0, "; ", tm1, "; ", tm2, ")")
)

(* ****** ****** *)

exception IllTyped0 of ()
exception IllTyped1 of (expr)
exception IllTyped2 of (expr, type)

(* ****** ****** *)

typedef tctx =
list0($tup(vnam, type))

(* ****** ****** *)

extern
fun oftype0(expr): type
extern
fun oftype1(tctx, expr): type

(* ****** ****** *)

val TPint = TPbas("int")
val TPstr = TPbas("string")
val TPbool = TPbas("bool")
val TPvoid = TPbas("void")

(* ****** ****** *)

implement
oftype0(tm0) =
oftype1(list0_nil(), tm0)

implement
oftype1(tctx0, tm0) =
(
case- tm0 of
//
| TMint _ => TPint
| TMstr _ => TPstr
//
(*
  | TMvar of vnam
*)
  | TMlam
    (x0, tp1, tm2) =>
    let
      val
      tctx1 =
      list0_cons
      ($tup(x0, tp1), tctx0)
    in
      TPfun(tp1, oftype1(tctx1, tm2))
    end
//
  | TMapp(tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    in
      case- tp1 of
      | TPfun(tp11, tp12) =>
        if
        tp11 = tp2
        then tp12 else $raise IllTyped0()
    end
//
  | TMifz(tm1, tm2, tm3) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    val tp3 = oftype1(tctx0, tm3)
    in
      if
      tp1 = TPint
      then
      (
      if tp2 = tp3 then tp2 else $raise IllTyped0()
      )
      else $raise IllTyped0()
    end
//
(*
  | TMfix of (vnam, vnam, expr) // Y(lam f.lam x.<body>)
//
  | TMopr of (opnm, exprlst)
//
*)
  | TMtup(tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    in
      TPtup(tp1, tp2)
    end
//
  | TMfst(tm1) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    in
      case- tp1 of TPtup(tp11, _) => tp11
    end
  | TMsnd(tm1) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    in
      case- tp1 of TPtup(_, tp12) => tp12
    end
//
  | TMlet(x0, tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tctx1 = list0_cons($tup(x0, tp1), tctx0)
    in
       oftype1(tctx1, tm2)
    end
)

(* ****** ****** *)

(*
val TMint1 = TMint(1)
val () =
println!("oftype(TMint1) = ", oftype0(TMint1))
val () =
println!("oftype(TMapp...) = ", oftype0(TMapp(TMint1, TMint1)))
*)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [lambda2.dats] *)

